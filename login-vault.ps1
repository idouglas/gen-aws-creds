param (
    [ValidatePattern("^http")]
    $vault_server = "https://vault.stanford.edu"
)

$env:VAULT_ADDR = "$vault_server"

$token_response = $(vault token renew -format=json 2>&1)
if (!($token_response.Exception)) { 
    $token_response = $token_response | ConvertFrom-Json
    $lease_expiration = ((get-date).AddSeconds($token_response.auth.lease_duration) | Out-String).Trim()
    Write-Host -ForegroundColor Cyan "Vault lease expires $lease_expiration (renewable)"
    break }

write-host -ForegroundColor white "`Logging into " -nonewline
write-host -ForegroundColor yellow "$env:VAULT_ADDR " -nonewline
write-host -ForegroundColor white "as " -nonewline
if ($env:USER) { 
    write-host -ForegroundColor yellow "$env:USER" -nonewline
    write-host -ForegroundColor white ". " -nonewline }
    else {
        write-host -ForegroundColor yellow "ENTER USERNAME" -nonewline 
    }
write-host -ForegroundColor white "Continue? (Y/N) / " -nonewline 
$confirm = read-host -prompt "<ENTER USERNAME>"

if ($confirm -imatch 'N'){ echo "exiting"; exit 1 }

elseif ($confirm -eq 'Y' -or $confirm -eq "") { $vault_username = $env:USER }

else { $vault_username = $confirm }

write-host -ForegroundColor Cyan "have Duo device ready"
$response = vault login -method=ldap username=$vault_username -format=json
$response = $response | ConvertFrom-Json
$lease_expiration = ((get-date).AddSeconds($response.auth.lease_duration) | Out-String).Trim()
Write-Host -ForegroundColor Cyan "Vault lease expires $lease_expiration (renewable)"

